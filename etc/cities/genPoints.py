#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import random

"""
Example of use:
python genPoints 20 300

* wygeneruje 20 miast
* x i y - będą z zakresu [0,300[
im mniejszy zakres tym mniejsze ogledłgości potem
i algo chodza płynniej
"""

if __name__ == '__main__':
	if len(sys.argv) != 3:
		print '2: ilosc miast'
		print '3: zakres siatki'
		sys.exit()
	N = int(sys.argv[1])
	maxRange = int(sys.argv[2])
	f = open(sys.argv[1] + '.range' + str(maxRange) + '.cities', 'w')
	tab = [[1 for i in range(2)] for j in range(N)]
	#for i in range(N):
	#	tab[i][i] = 0
	f.write('citiesAmount ' + str(N) + '\n')
	f.write('meshMaxRange ' + str(maxRange) + '\n')
	
	for i in range(N):
		tab[i][0] =  random.randrange(maxRange)
		tab[i][1] =  random.randrange(maxRange)
		f.write(str(tab[i][0]) + ' ' + str(tab[i][1]) + '\n')
		
	f.close()
