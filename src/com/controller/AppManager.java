package com.controller;

import com.gui.GuiManager;

public class AppManager extends Thread {

	@Override
	public void run() {
		new GuiManager().start();
	}
}
