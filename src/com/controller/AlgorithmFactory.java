package com.controller;

import java.util.ArrayList;

import com.algorithm.AlgorithmManager;
//import com.algorithm.TestAlgorithm.ExampleOfUse;
import com.algorithm.firefly.FireflyFactory;
import com.algorithm.pso.PSO;

public class AlgorithmFactory {
	public static AlgorithmManager getAlgorithm(String option,
			ArrayList<String> params, double[][] cities) {

		if (option.equals("pso")) {
			return checkCorrection(PSO.init(params, cities), params);
		} else if (option.equals("firefly")) {
			return checkCorrection(FireflyFactory.init(params, cities), params);
		} else {
			System.err.print("Leci null, factory nie zna takiego algorytmu!");
			return null;
		}
	}

	// remove this function
	private static AlgorithmManager checkCorrection(AlgorithmManager manager,
			ArrayList<String> params) {
		if (manager == null) {
			// System.err.print("Manager nullem(AlgorithmFactory),błąd:"
			// + params.get(0));
		}
		return manager;
	}
}