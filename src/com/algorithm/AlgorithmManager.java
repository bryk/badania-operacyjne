package com.algorithm;

import java.util.ArrayList;

public interface AlgorithmManager {

	/*
	 * wykonuje sie n-iteracji
	 */
	void step(int n);

	/*
	 * aktualny wynik po n-tej iteracji
	 */
	double getStepDistance();

	/*
	 * najlepszy wynik dla wszysikvh iteracji aktualizowany po każdym wywołaniu
	 * step
	 */
	double getBestDistance();

	/*
	 * najlepsza kolejność miast, aktualizowana po każdym wywołaniu step
	 */
	ArrayList<Integer> getBestCitiesOrder();

	/*
	 * w której iteracji był najlepszy wynik
	 */
	public int getBestSolutionIteration();
}

/*
 * Każdy algo musi zaimplementować tą statyczną metodę return null :znaczy że
 * błąd w danych wejściowych, wywalasz wszystko z listy i zostawiasz jeden
 * element będący komunikatem błędu argument ArrayList<String>, pojedynczy
 * element: "nazwa_parametru:wartość" argument tablica dwuwymiarowa odleglości
 * między miastami
 */
// public static AlgorithmManager init(ArrayList<String> parametry, double[][]
// cities);