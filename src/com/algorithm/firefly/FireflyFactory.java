package com.algorithm.firefly;

import java.util.ArrayList;

import com.algorithm.AlgorithmManager;

public class FireflyFactory {
	public static AlgorithmManager init(ArrayList<String> parametry,
			double[][] cities) {

		int numberOfFireflies = 0;
		double beta = 0;
		double gamma = 0;
		boolean error = false;

		for (String param : parametry) {
			try {
				if (param.startsWith("numberOfFireflies:")) {
					numberOfFireflies = Integer.parseInt(param.substring(param
							.indexOf(':') + 1));
				} else if (param.startsWith("beta:")) {
					beta = Float
							.parseFloat(param.substring(param.indexOf(':') + 1));
				} else if (param.startsWith("gamma:")) {
					gamma = Float
							.parseFloat(param.substring(param.indexOf(':') + 1));
				}
			} catch (NumberFormatException e) {
				error = true;
				break;
			}
		}
		parametry.clear();
		if (error == true) {
			parametry.add("Jeden z parametrow nie jest liczba");
			return null;
		}
		if (numberOfFireflies <= 0) {
			parametry.add("zla liczba swietlikow");
			error = true;
		}
		if (beta <= 0) {
			parametry.add("zly wspolczynnik beta");
			error = true;
		}
		if (gamma <= 0) {
			parametry.add("zly wspolczynnik gamma");
			error = true;
		}
		if (error == true)
			return null;

		return new FireflyAlgorithm(numberOfFireflies, beta, gamma, cities);
	}
}
