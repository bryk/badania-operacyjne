package com.algorithm.TestAlgorithm;

import java.util.ArrayList;
import java.util.Random;

import com.algorithm.AlgorithmManager;

/* odpalamy application, wypelniamy pierwsze pole-parameter1,
 klikamy start-wywoła init() i run(1) coś wypluje do konsoli
 klikamy stop-sprawdzi isFinished() wypisze getBest
 */
public class ExampleOfUse implements AlgorithmManager {
	// private boolean finished = false;

	public static AlgorithmManager init(ArrayList<String> params,
			double[][] cities) {
		if (!params.isEmpty()) {
			String param = params.get(0);
			// System.out.println(param);

			String[] result = param.split(":");
			return new ExampleOfUse(Double.parseDouble(result[1]));
		} else {
			System.out.println("array pusta");
			return new ExampleOfUse(123.45);
		}
	}

	public ExampleOfUse(double best) {
	}

	@Override
	public void step(int n) {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		// finished = true;
	}

	private static final Random random = new Random();

	@Override
	public double getStepDistance() {
		return 10 + random.nextInt(10);
	}

	@Override
	public double getBestDistance() {
		return 10 + random.nextInt(10);
	}

	@Override
	public ArrayList<Integer> getBestCitiesOrder() {
		ArrayList<Integer> t = new ArrayList<Integer>();
		t.add(new Integer(1));
		return t;
	}

	@Override
	public int getBestSolutionIteration() {
		return 10 + random.nextInt(10);
	}

}
